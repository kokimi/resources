IlamLUG Resources repo
Files, presentations and other resources are dumped in this repository.

Presentations
this is where the files for each presentation gets saved.
if you want to get a single file, you can use curl.
for examlpe:

```bash
curl 'https://gitlab.com/ilamlug/resources/raw/master/presentations/session_x/session_x.pdf'
```

logo
svg, png format of shiruzLUG logos in different colors.

pixel
modified version of the logos,circled for printing on pixels.